import java.awt.*;

/**
 * Un carr� qui peut �tre manipul� et qui se dessine lui-m�me sur un 'canvas'.
 * 
 * @author  Michael K�lling and David J. Barnes
 * @version 2011.07.31
 */

public class Square
{
    private int size;
    private int xPosition;
    private int yPosition;
    private String color;
    private boolean isVisible;

    /**
     * Cr�e un nouveau carr� � la position par d�faut
     * avec la couleur par d�faut.
     */
    public Square()
    {
        size = 60;
        xPosition = 310;
        yPosition = 120;
        color = "red";
        isVisible = false;
    }

    /**
     * Rend ce carr� visible. Si il �tait d�j� visible, ne fait rien.
     */
    public void makeVisible()
    {
        isVisible = true;
        draw();
    }
    
    /**
     * Rend ce carr� invisible. Si il �tait d�j� invisible, ne fait rien.
     */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    /**
     * D�place le carr� de quelques pixels � droite.
     */
    public void moveRight()
    {
        moveHorizontal(20);
    }

    /**
     * D�place le carr� de quelques pixels � gauche.
     */
    public void moveLeft()
    {
        moveHorizontal(-20);
    }

    /**
     * D�place le carr� de quelques pixels vers le haut.
     */
    public void moveUp()
    {
        moveVertical(-20);
    }

    /**
     * D�place le carr� de quelques pixels vers le bas.
     */
    public void moveDown()
    {
        moveVertical(20);
    }

    /**
     * D�place le carr� horizontalemnt de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    public void moveHorizontal(int distance)
    {
        erase();
        xPosition += distance;
        draw();
    }

    /**
     * D�place le carr� verticalement de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    public void moveVertical(int distance)
    {
        erase();
        yPosition += distance;
        draw();
    }

    /**
     * D�place horizontalement et lentement le carr�  de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    public void slowMoveHorizontal(int distance)
    {
        int delta;

        if(distance < 0) 
        {
            delta = -1;
            distance = -distance;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < distance; i++)
        {
            xPosition += delta;
            draw();
        }
    }

    /**
     * D�place vertcialement et lentement le carr�  de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    public void slowMoveVertical(int distance)
    {
        int delta;

        if(distance < 0) 
        {
            delta = -1;
            distance = -distance;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < distance; i++)
        {
            yPosition += delta;
            draw();
        }
    }

    /**
     * Remplace la taille actuelle du carr� par la nouvelle taille.
     *
     * @param newSize La nouvelle taille en pixels. Doit ^�tre &gt;= 0.
     */
    public void changeSize(int newSize)
    {
        erase();
        size = newSize;
        draw();
    }

    /**
     * Change la couleur.
     *
     * @param newColor La nouvelle couleur, les valeurs possibles sont "red",
     * "yellow", "blue", "green", "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }

    /**
     * Dessine le carr� � l'�cran avec les caract�ristiques actuelles.
     */
    private void draw()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.draw(this, color,
                        new Rectangle(xPosition, yPosition, size, size));
            canvas.wait(10);
        }
    }

    /**
     * Retire le carr� de l'�cran.
     */
    private void erase()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
