import java.awt.*;
import java.awt.geom.*;

/**
 * Un cercle qui peut �tre manipul� et qui se dessine lui-m�me sur un canvas.
 * 
 * @author  Michael K�lling and David J. Barnes
 * @version 2011.07.31
 */

public class Circle
{
    private int diameter;
    private int xPosition;
    private int yPosition;
    private String color;
    private boolean isVisible;
    
    /**
     * Cr�e un nouveau cercle de taille par d�faut, � la position par d�faut 
     * avec la couleur par d�faut.
     */
    public Circle()
    {
        diameter = 68;
        xPosition = 230;
        yPosition = 90;
        color = "blue";
    }

    /**
     * Rend ce cercle visible. Si il est d�j� visible, ne fait rien.
     */
    public void makeVisible()
    {
        isVisible = true;
        draw();
    }
    
    /**
     * Rend ce cercle invisible. Si il est d�j� invisible, ne fait rien.
     */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    /**
     * D�place le cercle de quelques pixels � droite.
     */
    public void moveRight()
    {
        moveHorizontal(20);
    }

    /**
     * D�place le cercle de quelques pixels � gauche.
     */
    public void moveLeft()
    {
        moveHorizontal(-20);
    }

    /**
     * D�placer le cercle de quelques pixels vers le haut.
     */
    public void moveUp()
    {
        moveVertical(-20);
    }

    /**
     * D�placer le cercle de quelques pixels vers le bas.
     */
    public void moveDown()
    {
        moveVertical(20);
    }

    /**
     * D�place le cercle horizontalement de 'distance' pixels.
     * 
     * @param distance La longueur du d�placement en pixels.
     */
    public void moveHorizontal(int distance)
    {
        erase();
        xPosition += distance;
        draw();
    }

    /**
     * D�place le cercle verticalement de 'distance' pixels.
     * 
     * @param distance La longueur du d�placement en pixels.
     */
    public void moveVertical(int distance)
    {
        erase();
        yPosition += distance;
        draw();
    }

    /**
     * D�place lentement et horizontalement le cercle de 'distance' pixels.
     * 
     * @param distance La longueur du d�placement en pixels.
     */
    public void slowMoveHorizontal(int distance)
    {
        int delta;

        if (distance < 0) 
        {
            delta = -1;
            distance = -distance;
        }
        else 
        {
            delta = 1;
        }

        for (int i = 0; i < distance; i++)
        {
            xPosition += delta;
            draw();
        }
    }

    /**
     * D�place lentement et verticalement le cercle de 'distance' pixels.
     * 
     * @param distance La longueur du d�placement en pixels.
     */
    public void slowMoveVertical(int distance)
    {
        int delta;

        if (distance < 0) 
        {
            delta = -1;
            distance = -distance;
        }
        else 
        {
            delta = 1;
        }

        for (int i = 0; i < distance; i++)
        {
            yPosition += delta;
            draw();
        }
    }

    /**
     * Change la taille vers la nouvelle taille (en pixels). 
     * 
     * @param newDiameter Le nouveau diam�tre en pixels, il doit �tre >= 0.
     */
    public void changeSize(int newDiameter)
    {
        erase();
        diameter = newDiameter;
        draw();
    }

    /**
     * Change la couleur.
     * 
     * @param newColor La nouvelle couleur, les valeurs possibles sont "red", 
     * "yellow", "blue", "green", "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }

    /**
     * Dessine le cercle � l'�cran avec les sp�cifications courantes.
     */
    private void draw()
    {
        if (isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.draw(this, color, new Ellipse2D.Double(xPosition, yPosition, 
                                                          diameter, diameter));
            canvas.wait(10);
        }
    }

    /**
     * Efface le cercle de l'�cran.
     */
    private void erase()
    {
        if (isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
